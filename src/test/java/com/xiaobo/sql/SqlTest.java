package com.xiaobo.sql;

import com.xiaobo.sql.component.column.OrdinaryColumn;
import com.xiaobo.sql.component.column.Sum;
import com.xiaobo.sql.component.condition.AbstractCondition;
import com.xiaobo.sql.component.condition.EqualCondition;
import com.xiaobo.sql.component.join.DefaultJoin;
import com.xiaobo.sql.component.query.QueryExpression;
import com.xiaobo.sql.component.query.Select;
import com.xiaobo.sql.component.table.OrdinaryTable;
import com.xiaobo.sql.enmus.JoinType;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author duxiaobo
 * @date 2021/11/2211:14 上午
 */
public class SqlTest {

    @Test
    void test1() {
        // 普通查询语句
        Select select = new Select();
        select.addColumn(new OrdinaryColumn("a", "a1").as("A1"))
                .addColumn(new OrdinaryColumn("b", "b1").as("B1"))
                // 新增子查询
                .addColumn(new OrdinaryTable(new Select()).as("t2"))
        .addColumn(new Sum("c","c1").as("sum_c"));
        select.addFrom(new OrdinaryTable("a")).addFrom(new OrdinaryTable("b"));
        // 表关联
        select.addJoins(new DefaultJoin(JoinType.left, new Select().as("t1"), "b", new OrdinaryColumn("a", "a1").as("A1"), new OrdinaryColumn("b", "b1").as("B1")));
        AbstractCondition condition = new EqualCondition(new OrdinaryColumn("1"), 1);
        select.setCondition(condition);
        System.out.println(select.toString());

    }

    @Test
    void test2(){
        QueryExpression queryExpression = new QueryExpression();
        queryExpression.setExpression("(${0}/${1})");
        Select select1 = new Select();
        select1.addColumn(new OrdinaryColumn("a","a1"));

        Select select2 = new Select();
        select2.addColumn(new OrdinaryColumn("b","b1"));
        List<String> contexts = Arrays.asList(select1.toSqlAsValue(),select2.toSqlAsValue());
        queryExpression.setContexts(contexts).as("t2");
        System.out.println(queryExpression.toString());


        Select select3 = new Select();
        select3.addJoins(new DefaultJoin(JoinType.right,queryExpression,"c",new OrdinaryColumn("a","a1").as("A"),new OrdinaryColumn("b","b1").as("B")));
        System.out.println(select3.toString());



    }


}
