package com.xiaobo.sql.base;

/**
 * 列接口
 * @author duxiaobo
 * @date 2021/11/244:42 下午
 */
public interface Column extends SqlString{
    String toSqlAsValue();
}
