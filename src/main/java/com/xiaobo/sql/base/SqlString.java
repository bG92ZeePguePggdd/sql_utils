package com.xiaobo.sql.base;

/**
 * sql接口
 * @author duxiaobo
 * @date 2021/11/244:41 下午
 */
public interface SqlString {
    void toSql(StringBuilder sql);
}
