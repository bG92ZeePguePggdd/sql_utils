package com.xiaobo.sql.base;

/**
 * 表接口
 * @author duxiaobo
 * @date 2021/11/245:00 下午
 */
public interface Table extends Expression{
    String toSqlToValueWithAS();
}
