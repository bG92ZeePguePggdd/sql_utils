package com.xiaobo.sql.component.join;

import com.xiaobo.sql.base.Column;
import com.xiaobo.sql.base.Join;
import com.xiaobo.sql.base.Table;
import com.xiaobo.sql.component.condition.AbstractCondition;
import com.xiaobo.sql.component.condition.EqualCondition;
import com.xiaobo.sql.enmus.JoinType;
import com.xiaobo.sql.utils.SqlUtils;

/**
 * 默认 表 连接类
 * @author duxiaobo
 * @date 2021/11/2511:27 上午
 */
public class DefaultJoin implements Join {


    private JoinType joinType;
    private Object srcTable;
    private Object targetTable;
    private Column srcColumn;
    private Column targetColumn;

    public DefaultJoin() {
    }

    public DefaultJoin(JoinType joinType, Object srcTable, Object targetTable, Column srcColumn, Column targetColumn) {
        this.joinType = joinType;
        this.srcTable = srcTable;
        this.targetTable = targetTable;
        this.srcColumn = srcColumn;
        this.targetColumn = targetColumn;
    }


    public JoinType getJoinType() {
        return joinType;
    }

    public DefaultJoin setJoinType(JoinType joinType) {
        this.joinType = joinType;
        return this;
    }

    public Object getSrcTable() {
        return srcTable;
    }

    public DefaultJoin setSrcTable(String srcTable) {
        this.srcTable = srcTable;
        return this;
    }

    public Object getTargetTable() {
        return targetTable;
    }

    public DefaultJoin setTargetTable(String targetTable) {
        this.targetTable = targetTable;
        return this;
    }

    public Column getSrcColumn() {
        return srcColumn;
    }

    public DefaultJoin setSrcColumn(Column srcColumn) {
        this.srcColumn = srcColumn;
        return this;
    }

    public Column getTargetColumn() {
        return targetColumn;
    }

    public DefaultJoin setTargetColumn(Column targetColumn) {
        this.targetColumn = targetColumn;
        return this;
    }

    @Override
    public void toSql(StringBuilder sql) {
        if (srcTable instanceof Table) {
            sql.append(((Table) srcTable).toSqlToValueWithAS());
        } else {
            SqlUtils.appendSymbol(srcTable, sql);
        }
        sql.append(joinType.getSymbol());
        if (targetTable instanceof Table) {
            sql.append(((Table) targetTable).toSqlToValueWithAS());
        } else {
            SqlUtils.appendSymbol(targetTable, sql);
        }
        sql.append(" ON ");
        AbstractCondition condition = new EqualCondition(srcColumn, targetColumn.toSqlAsValue());
        condition.toSql(sql);
    }
}
