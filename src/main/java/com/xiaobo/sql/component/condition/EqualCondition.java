package com.xiaobo.sql.component.condition;

import com.xiaobo.sql.base.Column;
import com.xiaobo.sql.component.condition.AbstractCondition;
import com.xiaobo.sql.enmus.ConditionOperatorType;

/**
 * 查询条件 等于
 * @author duxiaobo
 * @date 2021/11/2510:55 上午
 */
public class EqualCondition extends AbstractCondition {
    private Object value;

    public EqualCondition() {
    }

    public EqualCondition(Column column, Object value) {
        super(column);
        this.value = value;
    }

    @Override
    protected void subSQL(StringBuilder sql) {
        sql.append(column.toSqlAsValue());
        sql.append(ConditionOperatorType.eq.getSymbol());
        sql.append(value);
    }
}
