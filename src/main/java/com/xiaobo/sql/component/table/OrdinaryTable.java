package com.xiaobo.sql.component.table;

import com.xiaobo.sql.base.Table;
import com.xiaobo.sql.component.query.Select;
import com.xiaobo.sql.utils.SqlUtils;

import java.util.Objects;

/**
 * 默认 普通表
 * @author duxiaobo
 * @date 2021/11/245:06 下午
 */
public class OrdinaryTable implements Table {
    private Object table;
    private String aliasName;

    public OrdinaryTable() {
    }

    public OrdinaryTable(Object table) {
        this.table = table;
    }

    public OrdinaryTable(Object table, String aliasName) {
        this.table = table;
        this.aliasName = aliasName;
    }

    public OrdinaryTable(Select select, String aliasName) {
        this.table = select.toSqlAsValue();
        this.aliasName = aliasName;
    }

    public Object getTable() {
        return table;
    }

    public OrdinaryTable setTable(Select select) {
        this.table = select;
        return this;
    }

    public OrdinaryTable setTable(String table) {
        this.table = table;
        return this;
    }

    public String getAliasName() {
        return aliasName;
    }

    public OrdinaryTable setAliasName(String aliasName) {
        this.aliasName = aliasName;
        return this;
    }

    public OrdinaryTable as(String aliasName) {
        this.aliasName = aliasName;
        return this;
    }

    @Override
    public String toSqlAsValue() {
        StringBuilder sql = new StringBuilder();
        SqlUtils.appendSymbol(this.table, sql);
        return sql.toString();
    }

    @Override
    public void toSql(StringBuilder sql) {
        sql.append(toSqlAsValue());
        if (Objects.nonNull(this.aliasName)) {
            sql.append(" AS ");
            SqlUtils.appendSymbol(this.aliasName, sql);
        }
    }

    @Override
    public String toSqlToValueWithAS() {
        StringBuilder sql = new StringBuilder();
        toSql(sql);
        return sql.toString();
    }
}
