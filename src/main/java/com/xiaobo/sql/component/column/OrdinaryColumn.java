package com.xiaobo.sql.component.column;

import com.xiaobo.sql.base.Column;
import com.xiaobo.sql.utils.SqlUtils;

import java.util.Objects;

/**
 * 默认普通列
 * @author duxiaobo
 * @date 2021/11/244:48 下午
 */
public class OrdinaryColumn implements Column {
    private String tableName;
    private String columnName;
    private String aliasName;

    public OrdinaryColumn() {
    }

    public OrdinaryColumn(String columnName) {
        this.columnName = columnName;
    }

    public OrdinaryColumn(String tableName, String columnName) {
        this.tableName = tableName;
        this.columnName = columnName;
    }



    public String getTableName() {
        return tableName;
    }

    public OrdinaryColumn setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public String getColumnName() {
        return columnName;
    }

    public OrdinaryColumn setColumnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public String getAliasName() {
        return aliasName;
    }

    public OrdinaryColumn as(String aliasName) {
        this.aliasName = aliasName;
        return this;
    }


    @Override
    public String toSqlAsValue() {
        StringBuilder sql = new StringBuilder();
        if (Objects.nonNull(this.tableName)) {
            SqlUtils.appendSymbol(this.tableName, sql);
            sql.append(".");
        }
        SqlUtils.appendSymbol(this.columnName, sql);
        return sql.toString();
    }

    @Override
    public void toSql(StringBuilder sql) {
        sql.append(toSqlAsValue());
        if (Objects.nonNull(this.aliasName)) {
            sql.append(" AS ");
            SqlUtils.appendSymbol(this.aliasName, sql);
        }

    }

    @Override
    public String toString() {
        StringBuilder sql = new StringBuilder();
        toSql(sql);
        return sql.toString();
    }
}
