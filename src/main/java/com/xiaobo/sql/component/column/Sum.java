package com.xiaobo.sql.component.column;

import com.xiaobo.sql.base.Column;
import com.xiaobo.sql.utils.SqlUtils;

import java.util.Objects;

/**
 * @author duxiaobo
 * @date 2021/11/253:58 下午
 */
public class Sum implements Column {
    private String tableName;
    private String columnName;
    private String aliasName;

    public Sum() {
    }

    public Sum(String columnName) {
        this.columnName = columnName;
    }

    public Sum(String tableName, String columnName) {
        this.tableName = tableName;
        this.columnName = columnName;
    }

    public String getTableName() {
        return tableName;
    }

    public Sum setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public String getColumnName() {
        return columnName;
    }

    public Sum setColumnName(String columnName) {
        this.columnName = columnName;
        return this;
    }

    public String getAliasName() {
        return aliasName;
    }

    public Sum as(String aliasName) {
        this.aliasName = aliasName;
        return this;
    }

    @Override
    public String toSqlAsValue() {
        StringBuilder sql = new StringBuilder();
        sql.append(" SUM ( ");
        if (Objects.nonNull(this.tableName)) {
            SqlUtils.appendSymbol(this.tableName, sql);
            sql.append(".");
        }
        SqlUtils.appendSymbol(this.columnName, sql);
        sql.append(" ) ");
        return sql.toString();
    }

    @Override
    public void toSql(StringBuilder sql) {
        sql.append(toSqlAsValue());
        if (Objects.nonNull(this.aliasName)) {
            sql.append(" AS ");
            SqlUtils.appendSymbol(this.aliasName, sql);
        }
    }
}
