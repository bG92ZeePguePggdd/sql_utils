package com.xiaobo.sql.utils;

import com.xiaobo.sql.base.Expression;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * sql工具类
 *
 * @author duxiaobo
 * @date 2021/11/222:07 下午
 */
public class SqlUtils {

    public static void appendSymbol(Object obj, StringBuilder sql) {
        if (Objects.isNull(obj)) {
            return;
        }
        if (obj instanceof Expression) {
            sql.append(((Expression) obj).toSqlAsValue());
        } else {
            sql.append("`").append(obj).append("`");
        }
    }

    public static void appendBrackets(Object obj, StringBuilder sql) {
        if (Objects.isNull(obj)) {
            return;
        }
        sql.append("(").append(obj.toString()).append(")");
    }

    public static void appendValue(Object value, StringBuilder sql) {
        if (Objects.isNull(value)) {
            return;
        }

        if (value instanceof String) {
            sql.append("'");
            sql.append(value);
            sql.append("'");
        } else {
            sql.append(value);
        }

    }

}
