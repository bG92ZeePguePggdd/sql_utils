package com.xiaobo.sql.enmus;

/**
 * 逻辑运算枚举
 * @author duxiaobo
 * @date 2021/11/1911:26 上午
 */
public enum ConditionLogicType {
    and(0, " AND ", "且"),
    or(1, " OR ", "或");
    private final int code;
    private final String symbol;
    private final String desc;

    ConditionLogicType(int code, String symbol, String desc) {
        this.code = code;
        this.symbol = symbol;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getDesc() {
        return desc;
    }
}
