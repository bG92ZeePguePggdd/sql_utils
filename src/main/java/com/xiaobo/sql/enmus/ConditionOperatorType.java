package com.xiaobo.sql.enmus;

/**
 * 条件操作枚举
 * @author duxiaobo
 * @date 2021/11/1911:24 上午
 */
public enum ConditionOperatorType {
    eq(0," = ","等于");
    private final int code;
    private final String symbol;
    private final String desc;

    ConditionOperatorType(int code, String symbol, String desc) {
        this.code = code;
        this.symbol = symbol;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getDesc() {
        return desc;
    }
}
