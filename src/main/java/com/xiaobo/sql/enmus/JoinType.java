package com.xiaobo.sql.enmus;

/**
 * 连接类型枚举
 * @author duxiaobo
 * @date 2021/11/2511:38 上午
 */
public enum JoinType {
    left(0, " LEFT JOIN ", "左连接"),
    right(1, " RIGHT JOIN ", "右连接"),
    inner(2, " INNER JOIN ", "内连接"),
    full(3, " FULL JOIN ", "全连接"),
    ;
    private final int code;
    private final String symbol;
    private final String desc;

    JoinType(int code, String symbol, String desc) {
        this.code = code;
        this.symbol = symbol;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getDesc() {
        return desc;
    }
}
